import React from "react";
import { BallTriangle } from "react-loader-spinner";

import styles from "./TaskList.module.scss";

import useTask from "../../hooks/useTask/useTask";

import Task from "../Task/Task";

const TaskList = () => {
  const TASK_COUNT_PER_PAGE = 4;
  const { isDataLoaded, taskList } = useTask({
    taskCount: TASK_COUNT_PER_PAGE,
  });

  if (!isDataLoaded) {
    return (
      <div className={styles.loader_wrapper}>
        <BallTriangle color="#00BFFF" height={80} width={80} />
      </div>
    );
  }

  return (
    <div className={styles.task_list_wrapper}>
      {taskList.map((el, ind) => {
        return (
          <Task
            key={ind}
            header={el.header}
            task_description={el.task_description}
            task_tag_list={el.task_tag_list}
          />
        );
      })}
    </div>
  );
};

export default TaskList;
