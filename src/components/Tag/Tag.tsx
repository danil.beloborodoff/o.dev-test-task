import React from "react";
import styles from '../Tag/Tag.module.scss';

interface TagListTypes {
  text_content: string;
}

const TagList = (props: TagListTypes) => {
  return <span className={styles.tag}>{props.text_content}</span>
};

export default TagList;
