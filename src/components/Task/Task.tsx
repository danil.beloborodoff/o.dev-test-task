import React, { useState } from "react";

import styles from "./Task.module.scss";

import { TaskListType } from "../../hooks/useTask/useTask";
import Tag from "../Tag/Tag";

export const Task = (props: TaskListType) => {
  const [isCheckboxChecked, setIsCheckboxChecked] = useState(false);

  const handleCheckboxClick = () => {
    isCheckboxChecked
      ? setIsCheckboxChecked(false)
      : setIsCheckboxChecked(true);
  };
  return (
    <div className={styles.task}>
      <label
        className={`${styles.input_wrapper} ${isCheckboxChecked ? styles.input_wrapper__checked : ""}`}
      >
        <input
          onClick={handleCheckboxClick}
          className={styles.input}
          type="checkbox"
        />
      </label>
      <div className={styles.task__content_wrapper}>
        <div className={styles.task__text_content}>
          <p className={styles.header}>{props.header}</p>
          <p className={styles.description}>{props.task_description}</p>
        </div>
        <div className={styles.tag_wrapper}>
          {props.task_tag_list.map((el, ind) => (
            <Tag key={ind} text_content={el} />
          ))}
        </div>
      </div>
    </div>
  );
};

export default Task;
