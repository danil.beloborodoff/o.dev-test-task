import { useEffect, useState } from "react";

interface OptionsType {
  /**
   * Количество заданий, для которых будут сформированы данные
   */
  taskCount: number;
}

export interface TaskListType {
    header: string,
    task_description: string,
    task_tag_list: string[],
}

const useTask = (options: OptionsType) => {
  const [isDataLoaded, setIsDataLoaded] = useState(false);
  const [taskList, setTaskList] = useState([]);
  useEffect(() => {
    const createTaskList = () => {
      const taskList = [];
      const taskDataTemplate = {
        header: "Become the Dungeon Master",
        task_description:
          "Do the primary task or die! Do the primary task or die! Do the primary task or die! Do the primary task or die! Do the primary task or die! Do the primary task or die! Do the primary task or die!",
        task_tag_list: ["iOS"],
      };
      for (let i = 0; i < options.taskCount; i++) {
        taskList.push(taskDataTemplate);
      }
  
      return taskList;
    };

    const getTaskList = async () => {
      const taskList: any = await new Promise(res => window.setTimeout(() => res(createTaskList()), 2000));
      setIsDataLoaded(true);
      setTaskList(taskList);
    }

    getTaskList();
  }, []);

  return {
    isDataLoaded,
    taskList: taskList as TaskListType[],
  }
};

export default useTask;
